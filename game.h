#ifndef GAME_H
#define GAME_H

#include "block.h"
#include "glm.h"

#define W_GAME 10
#define H_GAME 20

class Game;
class Piece;

class Piece{
	public:
		void create_i_block(int x,int y,int value,Game *g);
		void create_o_block(int x,int y,int value,Game *g);
		void create_t_block(int x,int y,int value,Game *g);
		void create_s_block(int x,int y,int value,Game *g);
		void create_z_block(int x,int y,int value,Game *g);
		void create_j_block(int x,int y,int value,Game *g);
		void create_l_block(int x,int y,int value,Game *g);
		
		void setRotations(int r);
		int getRotations();

		void rotate(int d, Game *g);
		void move(int x,int y);
		void draw();

		Block b[4];
		int rotations;
};

class Game{
	public:
		Piece p;
		int create;
		int paused;
		int ttf;
		bool tictac;
		GLuint block_texture[7][2];

		Game();

		void load_block_textures();
		void draw();
		void create_piece();
		int move_piece(int x,int y);
		void drop();

		void fix_blocks();
	
		int check_collision(int x,int y);
		void check_elimination(int j,int i);
		void eliminate();
		void eliminate_line(int y);
		bool ticTacToe(int i,int j,int d,int range);
		bool withinBounds(int i,int j,int d,int range);
		bool fullLine(int i);
		bool emptyLine(int i);
		void drawmodel(char* path);
		void reset(void);
			
	private:
		Block matrix[W_GAME][H_GAME+5];
		int elimination[W_GAME][H_GAME+5];
		GLMmodel* pmodel=NULL;
};


#endif
