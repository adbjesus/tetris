#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

#include <iostream>
#include <vector>

#include <GL/gl.h>
#include <GL/glut.h>

#include "block.h"
#include "game.h"

#define W_SCREEN 700
#define H_SCREEN 700

Game g;
double inx,iny,radius,pitch,yaw;
int help;
int width;
int height;

void init(){
	glClearColor(0.1,0.1,0.1,1);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND); 	
	glShadeModel (GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	g.load_block_textures();
	g.create_piece();

	help=0;
	pitch = 1.21;
	yaw=-0.7;
	radius = 250;
	inx=50;
	iny=100;
}

void drawText(char *string, float x, float y, float z, void *font) {  
   glColor3f(1,1,1);
   glRasterPos3f(x,y,z);
   while (*string)
       glutBitmapCharacter(font, *string++);
}

void drawHelp() {
	glColor4f(0.0,0.0,0.0,1.0);
	glBegin(GL_QUADS);
		glVertex3i(-470, -310, 0); 
		glVertex3i(470, -310, 0); 
		glVertex3i(470, 310, 0); 
		glVertex3i(-470, 310, 0);
	glEnd();

	drawText(strdup("Help Menu"), -17.5, 95, 10, GLUT_BITMAP_HELVETICA_18);

	drawText(strdup("Use Left, Right and Down arrow keys to move piece around"),-40, 70, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("Use Up or End to rotate the piece and Spacebar to drop"),-40, 60, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("Fill in a row to delete it, and keep yourself from the top!"),-40, 50, 10, GLUT_BITMAP_HELVETICA_12);

	drawText(strdup("(w)   Increase Vertical Angle"), -25, 20, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(s)   Decrease Vertical Angle"), -25, 10, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(a)   Pan around to the Left"), -25, 0, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(d)   Pan around to the Right"), -25, -10, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(e)   Zoom out"), -25, -20, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(q)   Zoom in"), -25, -30, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(t)   Toggle Tic-Tac-Toe Mode"), -25, -50, 10, GLUT_BITMAP_HELVETICA_12);
	drawText(strdup("(n)   Start a new game"), -25, -60, 10, GLUT_BITMAP_HELVETICA_12);
}


void lights(){
	glDisable(GL_LIGHTING);
	/*Global ambient light*/
	GLfloat global_ambient[] = { 0.15f, 0.15f, 0.15f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
	/*So that light works well with lighting*/
	glLightModelf(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR);	

	// Create light components
	GLfloat diffuseLight[] = { 1.0f, 1.0f, 1.0, 1.0f };
	GLfloat specularLight[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat position0[] = { (GLfloat)inx, (GLfloat)(iny), 100.0f, 1.0f };
	GLfloat position1[] = { -10.f, (GLfloat)(iny), -70.0f, 1.0f };
	GLfloat position2[] = { (GLfloat)(inx*2+5.f), (GLfloat)(iny), -70.0f, 1.0f };
	GLfloat direction1[] = {1.0f, .0f, 1.0f };
	GLfloat direction2[] = {-1.0f, .0f, 1.0f };

	// Assign created components to GL_LIGHT0
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
	glLightf (GL_LIGHT0, GL_CONSTANT_ATTENUATION, 2.0f);
	glLightf (GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.0f);
	glLightf (GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.0f);
	glLightfv(GL_LIGHT0, GL_POSITION, position0);

	glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT1, GL_SPECULAR, specularLight);
	glLightf (GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.f);
	glLightf (GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.05f);
	glLightf (GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0f);
	glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 30.f);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
	glLightfv(GL_LIGHT1, GL_POSITION, position1);

	glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT2, GL_SPECULAR, specularLight);
	glLightf (GL_LIGHT2, GL_CONSTANT_ATTENUATION, 1.f);
	glLightf (GL_LIGHT2, GL_LINEAR_ATTENUATION, 0.05f);
	glLightf (GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.0f);
	glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, 30.f);
	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, direction2);
	glLightfv(GL_LIGHT2, GL_POSITION, position2);

	/*Objects*/
	float specReflection[] = { .6f, .6f, .6f, 1.0f };
	glMaterialfv(GL_FRONT, GL_SPECULAR, specReflection);
	glMateriali(GL_FRONT, GL_SHININESS, 80);
	
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHT0);	
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHTING);
}

void drawFloor(){
	int dim=9*BLOCK_SIZE+10*BLOCK_SPACING;
	double i;
	glPushMatrix();
		glColor4f(0.4,0.4,0.4,0.6);
		
		glBegin(GL_TRIANGLE_FAN);
			glNormal3f(0.0f,1.0f,0.0f);
			for(i=0;i<360;i+=360/50){
				glVertex3f(dim/2-BLOCK_SIZE/2+(dim*cos(i*3.14/180.0)/2),-0.1,(dim*sin(i*3.14/180.0))/2);
			}
		glEnd();
		/*
		glBegin(GL_QUADS);
			glNormal3f(0.0f,1.0f,0.0f);
			glVertex3f(dim,0,dim/2);
			glVertex3f(dim,0,-dim/2);
			glVertex3f(-BLOCK_SIZE,0,-dim/2);
			glVertex3f(-BLOCK_SIZE,0,dim/2);
		glEnd();
		*/
	glPopMatrix();
	return ;
}

void reflection(){
	glDepthMask(GL_FALSE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	glEnable( GL_STENCIL_TEST );

	// Draw floor
	glStencilFunc( GL_ALWAYS, 1, 0xFF ); // Set any stencil to 1
	glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
	glStencilMask( 0xFF ); // Write to stencil buffer
	glClear( GL_STENCIL_BUFFER_BIT ); // Clear stencil buffer (0 by default)

	/* Now drawing the floor just tags the floor pixels
		 as stencil value 1. */
	drawFloor();

	/* Re-enable update of color and depth. */ 
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);

	glStencilFunc( GL_EQUAL, 1, 0xFF ); // Pass test if stencil value is 1
	glStencilMask( 0x00 ); // Don't write anything to stencil buffer

	glPushMatrix();
		glScalef(1.0,-1.0,1.0);
		g.draw();
	glPopMatrix();

	glDisable(GL_STENCIL_TEST);
	return;

}

void display(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glEnable(GL_DEPTH_TEST);

	if(help) {
		glViewport(150, 150, width-300, height-300);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-110, 110, -110, 110, -110, 110);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();	
		gluLookAt(0, 0, 40, 0, 0, 0, 0, 1, 0);
		drawHelp();
	}

	else{
		glViewport (0, 0, (GLsizei) width, (GLsizei) height); 
		glMatrixMode (GL_PROJECTION);
		glLoadIdentity ();
		gluPerspective(60, 1.0, 0.01, 3000.0);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	 	gluLookAt(inx+(radius*sin(pitch)*sin(yaw)), iny+radius*cos(pitch),  radius*sin(pitch)*cos(yaw),
	 			  inx, iny, 0.0,
	 			  0.0, ((pitch>0)?1:-1), 0.0);
		if(iny+radius*cos(pitch)>0)
	 		reflection();
		drawFloor();
		lights();
		g.draw();
	}
	glutSwapBuffers();	
}

void reshape (int w, int h){
	width = w;
	height = h;
	/*glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);*/
	glutPostRedisplay();
}


void timer(int value){
	if(g.paused==1){
		glutTimerFunc(100,timer,0);
		return;
	}
	g.move_piece(0,-1);	
	glutPostRedisplay();
	glutTimerFunc(g.ttf,timer,0);
}

void Keyboard(unsigned char key,int x,int y){
	if(key=='h'){
		help=!help;
		g.paused=help;
		glutPostRedisplay();
	}
	if(help==1) return;
	if(key=='p'){
		g.paused=!g.paused;
	}
	if(key=='n'){
		if(g.paused==1)
			g.paused=0;
		g .reset();
		glutPostRedisplay();
	}
	if(key=='w'){
		pitch-=0.1;
		if(pitch<-3.14) pitch+=6.28;
		glutPostRedisplay();
	}
	if(key=='s'){
		pitch+=0.1;
		if(pitch>3.14) pitch-=6.28;
		glutPostRedisplay();
	}
	if(key=='a'){
		yaw-=0.1;
		if(yaw<-3.14) yaw+=6.28;
		glutPostRedisplay();
	}
	if(key=='d'){
		yaw+=0.1;
		if(yaw>3.14) yaw-=6.28;
		glutPostRedisplay();
	}
	if(key=='e'){
		radius-=5;
		glutPostRedisplay();
	}
	if(key=='q'){
		radius+=5;
		glutPostRedisplay();
	}
	if(key=='t'){
		g.tictac = !g.tictac;
	}
	if(g.paused==1) return;
	if(key==' '){
		g.drop();
		glutPostRedisplay();
	}

	
}

void SpecialKeys(int key,int x,int y){
	if(g.paused==1 || help==1) return;
	if(GLUT_KEY_RIGHT==key){
		g.move_piece(1,0);
		glutPostRedisplay();
	}
	if(GLUT_KEY_LEFT==key){
		g.move_piece(-1,0);
		glutPostRedisplay();
	}
	if(GLUT_KEY_DOWN==key){
		g.move_piece(0,-1);
		glutPostRedisplay();
	}
	if(GLUT_KEY_UP==key){
		g.p.rotate(1,&g);
		glutPostRedisplay();
	}
	if(GLUT_KEY_END==key){
		g.p.rotate(-1,&g);
		glutPostRedisplay();
	}
}


int main(int argc,char **argv){

	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
	glutInitWindowSize (W_SCREEN, H_SCREEN); 
	glutInitWindowPosition (100, 100); 
	glutCreateWindow ("Tetris");

	init();
	
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutTimerFunc(0,timer,0);
	glutKeyboardUpFunc(Keyboard);
	glutSpecialFunc(SpecialKeys);

	glutMainLoop();

	return 1;
}


