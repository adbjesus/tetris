FLAGS = -Wall -lGL -lglut -lGLU -g
EXEC = tetris

all: main.cpp
	g++ *.cpp $(FLAGS) -o $(EXEC)

run: $(EXEC)
	./$(EXEC)
