#include "block.h"

Block::Block(){
	color[0]=0;
	color[1]=0;
	color[2]=0;
	posX = 0;
	posY = 0;
	active = 0;
	symbol = 0;
}


void Block::draw(){
	double left,bot;
	double pos = (BLOCK_SIZE-BLOCK_SPACING)/2;

  left = posX * BLOCK_SIZE;
  bot = posY * BLOCK_SIZE;
	
	/*glColor4f(color[0],color[1],color[2],1);*/
  /*glColor4f(0.1,0.1,0.1,1);*/
	glColor4f(1.0,1.0,1.0,1);
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glTranslatef(left,bot,0);
	glTranslatef(pos,pos,-pos);
  glBindTexture(GL_TEXTURE_2D, tex);
	glBegin(GL_QUADS);
	  glNormal3f(0.0, 0.0, -1.0);
		glTexCoord2f(0,0);glVertex3f(-pos,-pos,-pos);
		glTexCoord2f(0,1);glVertex3f(-pos,pos,-pos);
		glTexCoord2f(1,1);glVertex3f(pos,pos,-pos);
		glTexCoord2f(1,0);glVertex3f(pos,-pos,-pos);

	  glNormal3f(0.0, 0.0, 1.0);
		glTexCoord2f(0,0);glVertex3f(-pos,-pos,pos);
		glTexCoord2f(0,1);glVertex3f(-pos,pos,pos);
		glTexCoord2f(1,1);glVertex3f(pos,pos,pos);
		glTexCoord2f(1,0);glVertex3f(pos,-pos,pos);
		
	  glNormal3f(-1.0, 0.0, 0.0);
		glTexCoord2f(0,0);glVertex3f(-pos,-pos,-pos);
		glTexCoord2f(0,1);glVertex3f(-pos,-pos,pos);
		glTexCoord2f(1,1);glVertex3f(-pos,pos,pos);
		glTexCoord2f(1,0);glVertex3f(-pos,pos,-pos);
		
	  glNormal3f(1.0, 0.0, 0.0);
		glTexCoord2f(0,0);glVertex3f(pos,-pos,-pos);
		glTexCoord2f(0,1);glVertex3f(pos,-pos,pos);
		glTexCoord2f(1,1);glVertex3f(pos,pos,pos);
		glTexCoord2f(1,0);glVertex3f(pos,pos,-pos);

	  glNormal3f(0.0, -1.0, 0.0);
		glTexCoord2f(0,0);glVertex3f(-pos,-pos,-pos);
		glTexCoord2f(0,1);glVertex3f(-pos,-pos,pos);
		glTexCoord2f(1,1);glVertex3f(pos,-pos,pos);
		glTexCoord2f(1,0);glVertex3f(pos,-pos,-pos);
	
	  glNormal3f(0.0, 1.0, 0.0);
		glTexCoord2f(0,0);glVertex3f(-pos,pos,-pos);
		glTexCoord2f(0,1);glVertex3f(-pos,pos,pos);
		glTexCoord2f(1,1);glVertex3f(pos,pos,pos);
		glTexCoord2f(1,0);glVertex3f(pos,pos,-pos);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	/*glutSolidCube(BLOCK_SIZE-BLOCK_SPACING);*/
	glPopMatrix();
}

void Block::setPos(int x,int y){
	posX = x;
	posY = y;
}

void Block::setSymbol(int x){
	symbol=x;
}

int Block::getSymbol(){
	return symbol;
}

int Block::getPosX(){
	return posX;
}

int Block::getPosY(){
	return posY;
}

void Block::setColor(double r,double g,double b){
	color[0]=r;
	color[1]=g;
	color[2]=b;
}

void Block::setTex(GLuint texture){
	tex = texture;
}

double Block::getRed(){
	return color[0];
}

double Block::getGreen(){
	return color[1];
}

double Block::getBlue(){
	return color[2];
}

int Block::getActive(){
	return active;
}

void Block::setActive(int a){
	active=a;
}

GLuint Block::getTex(){
	return tex;
}

