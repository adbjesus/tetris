#ifndef BLOCK_H
#define BLOCK_H

#define BLOCK_SIZE 10
#define BLOCK_SPACING 2

#include <GL/gl.h>
#include <GL/glut.h>
#include <time.h>
#include <stdlib.h>
#include "RgbImage.h"
#include <string.h>

class Block{
	public:
		Block();

		void draw();
	
		int getPosX();
		int getPosY();
		void setPos(int x,int y);
		void setColor(double r,double g,double b);
		void setTex(GLuint texture);
		GLuint getTex();
		double getRed();
		double getGreen();
		double getBlue();
		int getActive();
		void setActive(int a);
		void setSymbol(int x);
		int getSymbol();
	
	private:
		int posX,posY;
		double color[3];
		int active;
		int symbol;
		GLuint tex;

};


#endif
