#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define H 20
#define W 10

int dir[8][2]={{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1},{0,-1},{1,-1}};
char m[H][W];
char a[H][W];

int withinBounds(int i,int j,int d,int range);
int ticTacToe(int t,int j,int d,int range);
int checkBlock(int i,int j);
int fullLine(int i);

int main(){
  int i,j;
  for(i=0;i<H;i++){
    scanf("%s",m[i]);
    memset(a[i],' ',W);
  }

  for(i=0;i<H;i++){
    for(j=0;j<W;j++)
      printf("%c",m[i][j]);
    printf("\n");
  }
  
  for(i=0;i<H;i++)
    for(j=0;j<W;j++)
      if(m[i][j]!='.')
	checkBlock(i,j); /* usar esta função em todos os blocos da peca*/
  
  for(i=0;i<H;i++)       /* usar este ciclo para eliminar os blocos marcados */
    for(j=0;j<W;j++)
      if(a[i][j]=='+')
	m[i][j]='.';
  
  printf("\n");
  
  for(i=0;i<H;i++){
    for(j=0;j<W;j++)
      printf("%c",m[i][j]);
    printf("\n");
  }

  return 0;
}

int checkBlock(int i,int j){
  int d;
  for(d=0;d<8;d++){
    if(withinBounds(i,j,d,2))
      if(ticTacToe(i,j,d,2))
	a[i][j]=a[i+dir[d][0]][j+dir[d][1]]=
	  a[i+(2*dir[d][0])][j+(2*dir[d][1])]='+';
	
  }
    
  for(d=0;d<4;d++){
    if(withinBounds(i,j,d,-1)){
      if(ticTacToe(i,j,d,-1))
	a[i][j]=a[i+dir[d][0]][j+dir[d][1]]=
	  a[i+(-1*dir[d][0])][j+(-1*dir[d][1])]='+';
    }
  }
  
  
  if(fullLine(i))
    for(d=0;d<W;d++)
      a[i][d]='+';
  
  return 0;
}

int fullLine(int i){
  int d;
  for(d=0;d<W;d++)
    if(m[i][d]=='.')
      return 0;
  return 1;
}

int withinBounds(int i,int j,int d,int range){
  return (j+(range*dir[d][1])<W &&
	  j+(dir[d][1]) < W &&
	  j+(range*dir[d][1])>-1 &&
	  j+(dir[d][1]) > -1 &&
	  i+(range*dir[d][0])<H &&
	  i+(dir[d][0]) < H &&
	  i+(range*dir[d][0])>-1 &&
	  i+(dir[d][0]) > -1);
}

int ticTacToe(int i,int j,int d,int range){
  return(m[i][j]==m[i+dir[d][0]][j+dir[d][1]] &&
	 m[i][j]==m[i+(range*dir[d][0])][j+(range*dir[d][1])]);
}
