#include "block.h"
#include "game.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>
#include <GL/glu.h>

char combine[6][4]={{2,2,1,1},
					{2,1,1,2},
					{1,1,2,2},
					{1,2,1,2},
					{2,1,2,1},
					{1,2,2,1}};


int dir[8][2]={ {1 ,0},
				{1 ,1},
				{0 ,1},
				{-1,1},
				{-1,0},
				{-1,-1},
				{0 ,-1},
				{1 ,-1}
			};

void Piece::setRotations(int r){
	rotations=r;
}

int Piece::getRotations(){
	return rotations;
}

void Piece::create_i_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x+1,y);
	b[2].setPos(x+2,y);
	b[3].setPos(x-1,y);
	setRotations(2);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(0,0.7*v,1*v);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	 }
}

void Piece::create_o_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x,y+1);
	b[2].setPos(x+1,y);
	b[3].setPos(x+1,y+1);
	setRotations(0);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
		b[i].setColor(1*v,1*v,0);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::create_t_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x+1,y);
	b[2].setPos(x-1,y);
	b[3].setPos(x,y-1);
	setRotations(4);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(0.8*v,0,0.8*v);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::create_s_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x+1,y);
	b[2].setPos(x,y-1);
	b[3].setPos(x-1,y-1);
	setRotations(2);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(0,1*v,0);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::create_z_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x-1,y);
	b[2].setPos(x,y-1);
	b[3].setPos(x+1,y-1);
	setRotations(2);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(1*v,0,0);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::create_l_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x+1,y);
	b[2].setPos(x-1,y);
	b[3].setPos(x+1,y-1);
	setRotations(4);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(1*v,0.5*v,0);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::create_j_block(int x,int y,int value,Game *g){
	double v;
	b[0].setPos(x,y);
	b[1].setPos(x+1,y);
	b[2].setPos(x-1,y);
	b[3].setPos(x-1,y-1);
	setRotations(4);
	for(int i=0;i<4;i++){
		v=(b[i].getSymbol()==1)?0.5:1;
	 	b[i].setColor(0,0,1*v);
		b[i].setTex(g->block_texture[value][b[i].getSymbol()-1]);
	}
}

void Piece::move(int x,int y){
	int i;
	for(i=0;i<4;i++){
		b[i].setPos(b[i].getPosX()+x,b[i].getPosY()+y);
	}
}

void Piece::rotate(int d, Game *g){
	int i,newX,newY,mX=1,mY=1,r;
	Block v[4];
	for(i=0;i<4;i++)
		v[i]=b[i];
	r=getRotations();
	if(r==0) return;
	for(i=1;i<4;i++){
		newX=b[i].getPosY()-b[0].getPosY();
		newY=b[i].getPosX()-b[0].getPosX();
		if(r==-2 || r==2)
			setRotations(-r);
		if(r==2*-d){
			if(newX>=0) mX=d;
			else mX=-d;
			if(newY>=0) mY=-d;
			else mY=d;	
		}
		else{
			if(newX>=0) mX=-d;
			else mX=d;
			if(newY>=0) mY=d;
			else mY=-d;
		}
		b[i].setPos(b[0].getPosX()+(abs(newX)*mX),b[0].getPosY()+(abs(newY)*mY));		
	}
	if((*g).check_collision(0,0))
		for(i=0;i<4;i++)
			b[i]=v[i];
	
}

void Piece::draw(){
	int i;
	for(i=0;i<4;i++){
		b[i].draw();
	}
}


Game::Game(){
	paused = 0;
	tictac = false;
	ttf = 500;
	srand(time(NULL));
	for(int x=0;x<W_GAME;x++){
		for(int y=0;y<H_GAME;y++){
			matrix[x][y].setPos(x,y);
		}
	}
}


GLuint defineTexturas(char *str){   
	RgbImage  imag;
	GLuint tex;
	
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	imag.LoadBmpFile(str);
	
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, imag.GetNumCols(), imag.GetNumRows(), GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData() );

	return tex;
}

void Game::load_block_textures(){
	block_texture[0][0] = defineTexturas(strdup("img/o_cyan.bmp"));
	block_texture[1][0] = defineTexturas(strdup("img/o_yellow.bmp"));
	block_texture[2][0] = defineTexturas(strdup("img/o_magenta.bmp"));
	block_texture[3][0] = defineTexturas(strdup("img/o_green.bmp"));
	block_texture[4][0] = defineTexturas(strdup("img/o_red.bmp"));
	block_texture[5][0] = defineTexturas(strdup("img/o_blue.bmp"));
	block_texture[6][0] = defineTexturas(strdup("img/o_orange.bmp"));

	block_texture[0][1] = defineTexturas(strdup("img/x_cyan.bmp"));
	block_texture[1][1] = defineTexturas(strdup("img/x_yellow.bmp"));
	block_texture[2][1] = defineTexturas(strdup("img/x_magenta.bmp"));
	block_texture[3][1] = defineTexturas(strdup("img/x_green.bmp"));
	block_texture[4][1] = defineTexturas(strdup("img/x_red.bmp"));
	block_texture[5][1] = defineTexturas(strdup("img/x_blue.bmp"));
	block_texture[6][1] = defineTexturas(strdup("img/x_orange.bmp"));
}
void Game::drawmodel(char * path){
    if (!pmodel) {
        pmodel = glmReadOBJ(path);
        if (!pmodel) exit(0);
        glmUnitize(pmodel);
        glmFacetNormals(pmodel);
        glmVertexNormals(pmodel, 90.0);
    }
    
    glmDraw(pmodel, GLM_SMOOTH);
}

void Game::draw(){
	glPushMatrix();
		glTranslatef(0,1,0);
		p.draw();
		
		for(int x=0;x<W_GAME;x++){
			for(int y=0;y<H_GAME;y++){
				if(matrix[x][y].getActive()){
					matrix[x][y].draw();
				}
			}
		}
		glPushMatrix();

			glColor4f(1,1,1,1);
			GLfloat gridambient[] = {0,0,0};
			GLfloat griddiffuse[] = {0.1,0.1,0.1};
			GLfloat gridspecular[]=	{0.5,0.5,0.5};
			GLfloat gridshininess = 0.25;

			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, gridambient);
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, griddiffuse);
			glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, gridspecular);
			glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, gridshininess);

			glColor4f(1,1,1,1);
			glPushMatrix();
				glTranslatef(-BLOCK_SPACING,101,-4);
				glScalef(120,101,150);
				glRotatef(90,1,0,0);
				drawmodel(strdup("obj/Trelica.obj"));
			glPopMatrix();
			glPushMatrix();
				glTranslatef(W_GAME*BLOCK_SIZE,101,-4);
				glScalef(120,101,150);
				glRotatef(90,1,0,0);
				drawmodel(strdup("obj/Trelica.obj"));
			glPopMatrix();

			GLfloat ambient[] = {0,0,0,1};
			GLfloat diffuse[] = {0,0,0,1};
			GLfloat specular[]=	{0,0,0,1};

			glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
			glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
			glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
			glMateriali(GL_FRONT, GL_SHININESS, 0);
			
		glPopMatrix();
		glPushMatrix();
			glColor4f(1,1,1,.1);
			glTranslatef((W_GAME*BLOCK_SIZE)/2,(H_GAME*BLOCK_SIZE)/2,-BLOCK_SIZE/2+BLOCK_SPACING/2);
			glScalef(W_GAME*BLOCK_SIZE,H_GAME*BLOCK_SIZE,BLOCK_SIZE);
			glutSolidCube(1);
		glPopMatrix();
	glPopMatrix();
}

void Game::fix_blocks(){
	int i,x,y;
	for(i=0;i<4;i++){
		x = p.b[i].getPosX();
		y = p.b[i].getPosY();
		if(x < 0 || x>=W_GAME || y<0 || y>=H_GAME) continue;
		matrix[x][y].setActive(1);
		matrix[x][y].setColor(p.b[i].getRed(),p.b[i].getGreen(),p.b[i].getBlue());
		matrix[x][y].setSymbol(p.b[i].getSymbol());
		matrix[x][y].setTex(p.b[i].getTex());
	}
}

void Game::drop(){
	while(!move_piece(0,-1));
}

int Game::move_piece(int x,int y){
	int tmp = check_collision(x,y);
	int i,j;
	
	for(i=0;i<W_GAME;i++)
		for(j=0;j<H_GAME+5;j++)
			elimination[i][j]=0;
	
	if(tmp==1){
		for(i=0;i<4;i++){
			if(p.b[i].getPosY()>=H_GAME){
				paused = 1;
				return 1;
			}	
		}
		fix_blocks();
		for(i=0;i<4;i++)
			check_elimination(p.b[i].getPosX(),p.b[i].getPosY());
		eliminate();
		create_piece();
		return 1;
	}
	if(tmp==2){
		return 1;
	}
	p.move(x,y);
	return 0;
}

void Game::create_piece(){
	int i,r;
	r=random()%6;
	for(i=0;i<4;i++)
		p.b[i].setSymbol(combine[r][i]);
	switch(random()%7){
		case 0: p.create_i_block(5,20,0,this); break;
		case 1: p.create_o_block(4,20,1,this); break;
		case 2: p.create_t_block(4,21,2,this); break;
		case 3: p.create_s_block(4,21,3,this); break;
		case 4: p.create_z_block(4,21,4,this); break;
		case 5: p.create_j_block(5,21,5,this); break;
		case 6:	p.create_l_block(4,21,6,this); break;
	}
}

/*Return 1 means collision on bot
	Return 2 means collision on side*/
int Game::check_collision(int x,int y){
	int i;
	for(i=0;i<4;i++){
		if(p.b[i].getPosX()+x>=W_GAME) continue;
		if(p.b[i].getPosX()+x<0) continue;
		if(p.b[i].getPosY()+y<0) return 1;
		if(matrix[p.b[i].getPosX()][p.b[i].getPosY()+y].getActive()==1) return 1;
	}
	for(i=0;i<4;i++){
		if(p.b[i].getPosY()+y<0) continue;
		if(p.b[i].getPosX()+x>=W_GAME) return 2;
		if(p.b[i].getPosX()+x<0) return 2;
		if(matrix[p.b[i].getPosX()+x][p.b[i].getPosY()].getActive()==1) return 2;
	}
	
	return 0;
}

void Game::check_elimination(int x,int y){
	int d;
	 
	if(tictac){
		for(d=0;d<8;d++){
			if(withinBounds(x,y,d,2))
				if(ticTacToe(x,y,d,2)){
					elimination[x][y]=elimination[x+dir[d][0]][y+dir[d][1]]=
					elimination[x+(2*dir[d][0])][y+(2*dir[d][1])]=1;
				}	
		}
		
		for(d=0;d<4;d++){
			if(withinBounds(x,y,d,-1))
				if(ticTacToe(x,y,d,-1)){
					elimination[x][y]=elimination[x+dir[d][0]][y+dir[d][1]]=
					elimination[x+(-1*dir[d][0])][y+(-1*dir[d][1])]=1;
				}
		}
	}
	
    if(fullLine(y)){
    	for(d=0;d<W_GAME;d++)
      		elimination[d][y]=1;
    }
}

void Game::eliminate(){
	int x,y;
	for(y=0;y<H_GAME;y++){
		for(x=0;x<W_GAME;x++){
			if(elimination[x][y]==1)
				matrix[x][y].setActive(0);
		}
	}
	for(y=H_GAME;y>=0;y--)
		if(emptyLine(y))
			eliminate_line(y);
	
}

bool Game::fullLine(int i){
  int d;
  for(d=0;d<W_GAME;d++)
    if(!matrix[d][i].getActive())
      return false;
  return true;
}

bool Game::emptyLine(int i){
  int d;
  for(d=0;d<W_GAME;d++)
    if(matrix[d][i].getActive())
      return false;
  return true;
}

bool Game::withinBounds(int x,int y,int d,int range){
  	return (y+(range*dir[d][1])<H_GAME &&
		  	y+(dir[d][1]) < H_GAME &&
	  		y+(range*dir[d][1])>-1 &&
	  		y+(dir[d][1]) > -1 &&
	  		x+(range*dir[d][0])<W_GAME &&
	  		x+(dir[d][0]) < W_GAME &&
	  		x+(range*dir[d][0])>-1 &&
	  		x+(dir[d][0]) > -1);
}

bool Game::ticTacToe(int x,int y,int d,int range){
  return (
  		matrix[x][y].getActive() &&
  		matrix[x+dir[d][0]][y+dir[d][1]].getActive() &&
	 	matrix[x+(range*dir[d][0])][y+(range*dir[d][1])].getActive() &&
  		matrix[x][y].getSymbol()==matrix[x+dir[d][0]][y+dir[d][1]].getSymbol() &&
	 	matrix[x][y].getSymbol()==matrix[x+(range*dir[d][0])][y+(range*dir[d][1])].getSymbol());
}

void Game::eliminate_line(int y){
	int x;
	y = y+1;
	if(y<H_GAME){
		for(x=0;x<W_GAME;x++){
			matrix[x][y-1].setActive(matrix[x][y].getActive());
			matrix[x][y-1].setColor(matrix[x][y].getRed(),matrix[x][y].getGreen(),matrix[x][y].getBlue());
			matrix[x][y-1].setSymbol(matrix[x][y].getSymbol());
			matrix[x][y-1].setTex(matrix[x][y].getTex());
		}
		eliminate_line(y);
	}
	else
		for(x=0;x<W_GAME;x++)
			matrix[x][y-1].setActive(0);

}

void Game::reset(void){
	int i;
	for(i=0;i<H_GAME;i++)
		eliminate_line(0);
	create_piece();
	return;
}


